module BayesianScore

using DataFrames
using Dates
using LightGraphs
using Printf
using SpecialFunctions

export Stats, build_stats, 
    Counter, build_counter, increment_counters, parent_instantiations_count,
    compute_bayesian_score, K2Context

"""
Store statistics about the data being processed.
"""
struct Stats
    # number of variables/nodes
    n::Int
    # number of instations of each variables
    r::Array{Int}
    # Map from indexes to node names
    names::Array{String}
end

function build_stats(df::DataFrame)::Stats 
    # number of variables
    n = size(df, 2)
    # number of instantiation of each variable
    r = zeros(Int, n)
    for i in 1:n
        r[i] = maximum(df[i])
    end

    idx2names = [string(n) for n in names(df)]

    Stats(n, r, idx2names)
end

"""
Keep track of the counters for a particular dag. 
The size of the datastructures is dependent of the exact Dag.
"""
struct Counter
    # Vector holding one variable dimension array for each node.
    m::Array{Any, 1}

    # Store the Dag for convenience
    dag::DiGraph

    # Store the data stats for convenience
    stats::Stats
end

function build_counter(dag::DiGraph, stats::Stats)::Counter
    m = Array{Any, 1}()
    # for each node
    for i in 1:stats.n
        # build dimensions for parents
        dims = [stats.r[p] for p in inneighbors(dag,i)]
        # add one last dimension for the node's own instantations
        push!(dims, stats.r[i])
        # build the array for the given node
        push!(m, zeros(Float64, dims...))
    end
    Counter(m, dag, stats)
end

"""
Increment counters for the given row of data.
"""
function increment_counters(counter::Counter, row::Array{Union{Missing,Int}})
    dag = counter.dag
    for node in 1:counter.stats.n
        # build the parent indices that need to be incremented
        indices = [row[p] for p in inneighbors(dag, node)]
        # add the index for the value of the current node
        push!(indices, row[node])
        # get the accumulator for the current node
        acc = counter.m[node]
        # increment the counter at the index
        try 
            acc[indices...] += 1
        catch err
            @show err
            @show node, counter.stats.r[node], row[node]
            @show row
            @show size(acc)
            error("boom")
        end
    end
end

function compute_bayesian_score(dag::DiGraph, stats::BayesianScore.Stats, df::DataFrame)::Float64
    counter = build_counter(dag, stats)
    for d in eachrow(df)
        row = vec(convert(Array, d))
        increment_counters(counter, row)
    end
    score = 0.0
    for i in 1:stats.n
        acc = counter.m[i]
        # build an index iterator for the parents
        parent_max = [stats.r[p] for p in inneighbors(dag, i)]
        indices = CartesianIndices(Tuple(parent_max))

        # iterate over all the parents
        for j in indices
            r_i = stats.r[i]
            m_ij0 = 0.0
            sum_ri = 0.0
            for k in 1:r_i
                m_ijk = acc[j,k]
                m_ij0 += m_ijk
                sum_ri += lgamma(1+m_ijk)
            end
            score += lgamma(r_i) - lgamma(r_i + m_ij0) + sum_ri
        end
    end
    score
end

"""
Data needed to run K2 iterations
"""
struct K2Context
    df::DataFrame
    stats::BayesianScore.Stats
    max_parents::Int
    dag::DiGraph
end


end