# node  index 1 with 4 instantiations and 2 parents
#  - index 2 has 3 instantiations
#  - index 3 has 2instantiations

indices = [3,2,4]
acc = zeros(Float64, indices...)

# row with data, in the order of the indices
row = [4, 3, 2]

# put in the order of the indicies

right = [3,2,4]

acc[right...] += 1

# iterate just over the parents
for i in eachindex(view(acc))
    @show i
    @show acc[i...]
end