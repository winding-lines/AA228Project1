using Pkg
Pkg.activate(@__DIR__)

using ArgParse
using CSV
using Dates
using DataFrames
using LightGraphs
using Logging
using Printf
using TikzGraphs
using TikzPictures 

include("BayesianScore.jl")

"""
    write_gph(dag::DiGraph, idx2names, filename)

Takes a DiGraph, a Dict of index to names and a output filename to write the graph in `gph` format.
"""
function write_gph(dag::DiGraph, idx2names::Array{String}, filename)
    open(filename, "w") do io
        for edge in edges(dag)
            @printf(io, "%s, %s\n", idx2names[src(edge)], idx2names[dst(edge)])
        end
    end

    plot = TikzGraphs.plot(dag, idx2names)
    TikzPictures.save(PDF(filename * ".pdf"), plot)
end

function k2_score(k2::BayesianScore.K2Context)::Float64
    BayesianScore.compute_bayesian_score(k2.dag, k2.stats, k2.df)
end

"""
Add the best parents to the given node as defined by parents that lower the score.
Do not add more than max_parents.
"""
function add_parents(node::Int, k2::BayesianScore.K2Context, scores::Array{Float64,1})::DiGraph
    # @printf("%s, adding parents, %d\n", now(), node)
    best_score = k2_score(k2)
    push!(scores, best_score)

    # build a list of candidates
    candidates = setdiff(Set(1:k2.stats.n), Set(inneighbors(k2.dag, node)))
    delete!(candidates, node)

    while size(inneighbors(k2.dag, node),1) < k2.max_parents && !isempty(candidates) 

        # add a parent, unless this creates a cyclic graph
        one = pop!(candidates)
        add_edge!(k2.dag, one, node)
        if !is_cyclic(k2.dag)
            # evalute if better dag
            current = k2_score(k2)
            # @printf("%s, eval parent, %d,%d, %f\n", now(), node, one, current)
            if current > best_score
                # keep the current dag and update the score
                best_score = current
                push!(scores, best_score)
                continue
            end
        end

        rem_edge!(k2.dag, one, node)
    end
    k2.dag
end

struct K2Result
    dag::DiGraph
    scores::Array{Float64,1}
end

"""
Implement the K2 Bayesian structure search algorithm.
"""
function K2_run(df::DataFrame, stats::BayesianScore.Stats, max_parents::Int)::K2Result

    # dag to work on
    dag = DiGraph(stats.n)
    context = BayesianScore.K2Context(df, stats, max_parents, dag) 
    
    # store the scores for each iteration
    scores = Array{Float64, 1}()
    
    # Main loop in K2
    for node in 1:stats.n
        dag = add_parents(node, context, scores)
    end
    return K2Result(dag, scores)
end

"""
Try different values of the parent counts.
"""
function learn_parent_count(infile, outfile, max_parents::Int)
    start = now()
    df = CSV.read(infile)
    stats = BayesianScore.build_stats(df)
    total_scores = 0
    for parents in 1:max_parents
        result = K2_run(df, stats, parents)
        @printf("%s, %d, %d, %f, %s\n", infile, parents, size(result.scores,1), last(result.scores), Dates.CompoundPeriod(now()-start) )
        if size(result.scores, 1) == total_scores 
            write_gph(result.dag, stats.names, outfile )
            break
        end
        total_scores = size(result.scores,1)
    end
end

"""
Top level processing files.
"""
function compute(infile, outfile, parents)

    df = CSV.read(infile)
    dropmissing(df)
    start = now()
    stats = BayesianScore.build_stats(df)
    result = K2_run(df, stats, parents)
    @info @sprintf("%s, %f, %s\n", infile, last(result.scores), Dates.CompoundPeriod(now()-start) )
   
    write_gph(result.dag, stats.names, outfile)
end


if length(ARGS) < 2
    @printf("usage: julia project1.jl <infile>.csv <outfile>.gph")
else
    io = open("log.txt", "w+")
    # Create a simple logger
    logger = SimpleLogger(io)
    global_logger(logger)    
    
    # Set the global logger to logger
    s = ArgParseSettings()
    @add_arg_table s begin
        "--parents"
            help = "maximum number of parents"
            arg_type = Int
            default = 2
        "inputfilename"
            help = "the name of the input file"
            required = true
        "outputfilename"
            help = "the name of the output file"
            required = true
    end
    parsed = parse_args(ARGS, s)

    @info @sprintf("Processing %s, maximum parents %d\n", parsed["inputfilename"], parsed["parents"])
    compute(parsed["inputfilename"], parsed["outputfilename"], parsed["parents"])
    flush(io)
end

